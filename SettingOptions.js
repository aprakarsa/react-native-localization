export const options = [
  {
    id: 0,
    option: 'Update Profile',
  },
  {
    id: 1,
    option: 'Update Address',
  },
  {
    id: 2,
    option: 'Update Notification Preference',
  },
  {
    id: 3,
    option: 'Sign Out',
  },
];
