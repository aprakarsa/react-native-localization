import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import React from 'react';

const SettingsOptions = ({label}) => {
  return (
    <TouchableOpacity>
      <Text style={styles.text}>{label}</Text>
    </TouchableOpacity>
  );
};

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <View>
          <Text style={[styles.text, styles.header]}>Settings</Text>
        </View>
        <SettingsOptions label={'Update Profile'} />
        <SettingsOptions label={'Update Address'} />
        <SettingsOptions label={'Update Notofication Preference'} />
        <SettingsOptions label={'Sign Out'} />
      </View>
      <Text style={styles.text}>App</Text>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#102D66',
  },
  content: {
    paddingHorizontal: 20,
  },
  text: {
    color: 'white',
    fontSize: 15,
  },
  header: {
    fontSize: 30,
    fontWeight: 'bold',
    marginBottom: 40,
  },
});
